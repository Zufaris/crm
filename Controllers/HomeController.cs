﻿using Microsoft.AspNetCore.Mvc;
using testing_empty_ASP.Models;

namespace testing_empty_ASP.Controllers
{
    public class HomeController : Controller
    {
        public ViewResult Index()
        {
            var company = new[] {new Company(){PublicName = "Поляна", SubNames = new List<string>() {"Фиалка", "Гортензия", "Гвоздика" } },
                                 new Company(){PublicName = "Etalon ltd", SubNames = new List<string>() { "Гиацинт", "Декабрист", "Хризантема" }},
                                 new Company(){PublicName = "RosTeleCom", SubNames = new List < string >() { "Василёк", "Одуванчик", "Расторопша" }}
                                };
            return View(company);
        }
    }
}
