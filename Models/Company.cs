﻿namespace testing_empty_ASP.Models
{
    public class Company
    {
        int? id;
        static int count = 0;
        string? publicName;
        int parentId = 0;
        string? description;
        List<string>? subNames;
        string? branch;
        string? wwwAdress;
        int? potential;

        public int? Id { get => id; set => id = value; }
        public string PublicName { get => publicName; set => publicName = value; }
        public List<string>? SubNames { get => subNames; set => subNames = value; }
        public string? Branch { get => branch; set => branch = value; }
        public string? WwwAdress { get => wwwAdress; set => wwwAdress = value; }
        public int? Potential { get => potential; set => potential = value; }
        public int ParentId { get => parentId; set => parentId = value; }
        public virtual Manager Manager { get; set; }
        public string? Description { get => description; set => description = value; }

        public Company() => this.id = ++count;
                
        public override string ToString()
        {
            return $"Company: {PublicName}\tOther names: {SubNames}";
        }
    }
}
