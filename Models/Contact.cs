﻿namespace testing_empty_ASP.Models
{
    public class Contact
    {
        public int Id { get; set; }
        DateTime day { get; set; }
        byte? curType { get; set; } //0 - phone, 1 - email, 2 - messanger, 3 - face to face
        string? Description { get; set; }
        DateTime? NextContact { get; set; }
        public virtual Person Person { get; set; }
        public virtual Manager Manager { get; set; }
        static int count = 0;
        public Contact(Person person, Manager manager)
        {
            Id = ++count;
            this.day = DateTime.Now;
            Person = person;
            Manager = manager;
        }
    }
}
