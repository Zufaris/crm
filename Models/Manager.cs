﻿namespace testing_empty_ASP.Models
{
    public class Manager
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SecondName { get; set; }
        public string Description { get; set; }
        static int count = 0;
        public Manager() => Id = ++ count;
    }
}
