﻿namespace testing_empty_ASP.Models
{
    public class Person
    {
        int id;
        static int count = 0;
        string? name;
        string? secondName;
        string? description;
        string? phone;
        string? email;
        DateTime? birthDate;
        DateTime? lastContacted;
        Company? company;
        public int Id { get { return id; } set { id = value; } }
        public string? Name { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set;}
        public string? Email { get; set; }
        public string? Phone { get { return phone; } set { phone = value; } }
        public DateTime? BirthDate { get {  return birthDate; } set {  birthDate = value; } }
        public virtual Company Company { get { return company; } set { company = value; } }
        public string? Description { get => description; set => description = value; }
        public Person() => id = ++count;
    }

        
}
